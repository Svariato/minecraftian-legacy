package com.gitlab.svariato.minecraftianlegacy.item.legacies;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public abstract class ActivatableLegacy extends Item
{
    public ActivatableLegacy(Item.Properties properties) { super(properties); }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand)
    {
        ItemStack stack = player.getHeldItem(hand).getStack();
        setActive(stack, player, !isActive(stack)); // Toggle active.
        return new ActionResult<>(ActionResultType.PASS, player.getHeldItem(hand));
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack stack, PlayerEntity player)
    {
        if (isActive(stack)) setActive(stack, player, false);
        return super.onDroppedByPlayer(stack, player);
    }

    protected boolean canBeActivated(PlayerEntity player, ItemStack legacyItemStack)
    {
        // If trying to activate a legacy, make sure there are no others already activated in the inventory.
        for (int i = 0; i < player.inventory.getSizeInventory(); i++)
        {
            // If there is an active legacy already in the inventory, skip this activation request.
            ItemStack inventoryStack = player.inventory.getStackInSlot(i);
            if (inventoryStack == legacyItemStack) continue;
            if (isActive(inventoryStack)) return false;
        }

        return true;
    }

    protected abstract void onActivate(PlayerEntity player, ItemStack legacyItemStack);
    protected abstract void onDeactivate(PlayerEntity player, ItemStack legacyItemStack);

    public static void setActive(ItemStack legacyItemStack, PlayerEntity player, boolean active)
    {
        if (!(legacyItemStack.getItem() instanceof ActivatableLegacy)) return;

        ActivatableLegacy activatableLegacy = (ActivatableLegacy)legacyItemStack.getItem();

        // If trying to activate a legacy, make sure it actually can be activated.
        if (active && !activatableLegacy.canBeActivated(player, legacyItemStack)) return;

        if (!legacyItemStack.hasTag())
        {
            legacyItemStack.setTag(new CompoundNBT());
            legacyItemStack.getTag().putBoolean("legacy_is_active", active);

            // Since the legacies are initially deactivated, we only need to check for a call to onActivate.
            if (active) activatableLegacy.onActivate(player, legacyItemStack);
        }
        // Only set the tag and call the activate/deactivate functions if the state would actually change.
        else if (!(legacyItemStack.getTag().contains("legacy_is_active") && legacyItemStack.getTag().getBoolean("legacy_is_active") == active))
        {
            legacyItemStack.getTag().putBoolean("legacy_is_active", active);
            if (active) activatableLegacy.onActivate(player, legacyItemStack);
            else activatableLegacy.onDeactivate(player, legacyItemStack);
        }
    }

    public static boolean isActive(ItemStack legacyItemStack)
    {
        return legacyItemStack.getItem() instanceof ActivatableLegacy
                && legacyItemStack.hasTag()
                && legacyItemStack.getTag().getBoolean("legacy_is_active");
    }
}
