package com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer;

import com.gitlab.svariato.minecraftianlegacy.item.ModItems;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;

public class ItemInertLegacySlayer extends Item
{
    private static final int KILLS_REQUIRED_TO_TRANSFORM = 25; // TODO: Add a progress indicator. Don't want players to stop trying cause they think it's broken.
    private static final Item TRANSFORMED_ITEM = ModItems.LEGACY_SLAYER.get();

    public ItemInertLegacySlayer(Item.Properties properties)
    {
        super(properties);
    }

    /**
     * LivingDeathEvent listener that tracks the number of monsters killed while the
     * Inert Legacy of the Slayer is in the off-hand. Once the threshold for transformation
     * is reached, the item transforms into Legacy of the Slayer.
     */
    public static void onLivingDeath(LivingDeathEvent event)
    {
        if (!(event.getSource().getTrueSource() instanceof PlayerEntity) || !(event.getEntityLiving() instanceof MonsterEntity)) return;

        PlayerEntity sourcePlayer = (PlayerEntity) event.getSource().getTrueSource();
        ItemStack stack = sourcePlayer.getHeldItem(Hand.OFF_HAND);

        if (stack.getItem() instanceof ItemInertLegacySlayer)
        {
            if (!stack.hasTag()) stack.setTag(new CompoundNBT());

            if (stack.getTag().contains("ILS_mob_kill_count"))
            {
                int newKillCount = stack.getTag().getInt("ILS_mob_kill_count") + 1;
                if (newKillCount == KILLS_REQUIRED_TO_TRANSFORM) sourcePlayer.setHeldItem(Hand.OFF_HAND, TRANSFORMED_ITEM.getDefaultInstance());
                else stack.getTag().putInt("ILS_mob_kill_count", newKillCount);
            }
            else stack.getTag().putInt("ILS_mob_kill_count", 1);
        }
    }
}
