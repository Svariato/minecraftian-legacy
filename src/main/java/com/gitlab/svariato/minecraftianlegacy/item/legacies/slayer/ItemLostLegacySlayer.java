package com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer;

import com.gitlab.svariato.minecraftianlegacy.item.ItemLostLegacyWithContainer;
import com.gitlab.svariato.minecraftianlegacy.item.ModItems;
import net.minecraft.item.Item;
import net.minecraft.item.Items;

public class ItemLostLegacySlayer extends ItemLostLegacyWithContainer
{
    private static final Item[] ITEMS_TO_COMPLETE = {
            Items.ROTTEN_FLESH,
            Items.BONE,
            Items.SPIDER_EYE,
            Items.GUNPOWDER,
            Items.ENDER_PEARL
    };

    public ItemLostLegacySlayer(Item.Properties properties)
    {
        super(properties, ModItems.INERTLEGACY_SLAYER.get(), ITEMS_TO_COMPLETE);
    }
}
