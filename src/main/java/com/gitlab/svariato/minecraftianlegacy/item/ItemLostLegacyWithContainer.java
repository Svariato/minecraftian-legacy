package com.gitlab.svariato.minecraftianlegacy.item;

import com.gitlab.svariato.minecraftianlegacy.container.ContainerLostLegacy;
import com.gitlab.svariato.minecraftianlegacy.util.RestrictedItemStackHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

/**
 * A Lost Legacy item that, when right-clicked in hand, opens a Container GUI that accepts
 * specific items. Once the GUI is close with all slots filled, it will transform into an
 * Inert Legacy.
 */

public class ItemLostLegacyWithContainer extends ItemLostLegacy implements INamedContainerProvider
{
    private final Item[] itemsToComplete;
    private final Item transformedItem; // Item that this is turned into when completed.

    //@Deprecated
    public ItemLostLegacyWithContainer(Item.Properties properties, Item... itemsToComplete)
    {
        super(properties);
        this.itemsToComplete = itemsToComplete;
        transformedItem = null;
    }

    public ItemLostLegacyWithContainer(Item.Properties properties, Item transformedItem, Item... itemsToComplete)
    {
        super(properties);
        this.itemsToComplete = itemsToComplete;
        this.transformedItem = transformedItem;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand)
    {
        if (!world.isRemote) NetworkHooks.openGui((ServerPlayerEntity) player, this);
        return new ActionResult<>(ActionResultType.PASS, player.getHeldItem(hand));
    }

    @Override
    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity player)
    {
        return new ContainerLostLegacy(id, playerInventory);
    }

    @Override
    public ITextComponent getDisplayName()
    {
        return new TranslationTextComponent("screen.minecraftianlegacy.lost_legacy");
    }

    public Item getTransformedItem() { return transformedItem; }

    public static RestrictedItemStackHandler getHandlerForContainer(ItemStack stack)
    {
        if (stack.isEmpty() || !(stack.getItem() instanceof ItemLostLegacyWithContainer)) return null;
        RestrictedItemStackHandler handler = new RestrictedItemStackHandler(((ItemLostLegacyWithContainer)stack.getItem()).itemsToComplete);
        if (stack.hasTag()) handler.deserializeNBT(stack.getTag().getCompound("lost_legacy_container_inventory"));
        return handler;
    }
}