package com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector;

import net.minecraft.item.Item;

/**
 * This item transforms into Legacy of the Prospector after it is smelted
 * for 1600 ticks (the time it takes for a single piece of coal to burn).
 */
public class ItemInertLegacyProspector extends Item
{
    public ItemInertLegacyProspector(Item.Properties properties)
    {
        super(properties);
    }
}
