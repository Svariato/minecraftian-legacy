package com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector;

import com.gitlab.svariato.minecraftianlegacy.effect.ModEffects;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.ActivatableLegacy;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import org.apache.logging.log4j.LogManager;

public class ItemLegacyProspector extends ActivatableLegacy
{
    public ItemLegacyProspector(Item.Properties properties)
    {
        super(properties);
    }

    @Override
    protected void onActivate(PlayerEntity player, ItemStack legacyItemStack)
    {
        LogManager.getLogger().debug("Activated Legacy of the Prospector.");

        EffectInstance effect = new EffectInstance(ModEffects.PROSPECTOR_EFFECT, Integer.MAX_VALUE, 0);
        player.addPotionEffect(effect);

        if (!legacyItemStack.hasTag()) legacyItemStack.setTag(new CompoundNBT());
        legacyItemStack.getTag().put("prospector_effect", effect.write(new CompoundNBT()));
    }

    @Override
    protected void onDeactivate(PlayerEntity player, ItemStack legacyItemStack)
    {
        LogManager.getLogger().debug("Deactivated Legacy of the Prospector.");

        player.removePotionEffect(EffectInstance.read(legacyItemStack.getTag().getCompound("prospector_effect")).getPotion());
    }

    @Override
    public boolean hasEffect(ItemStack stack) { return true; }
}
