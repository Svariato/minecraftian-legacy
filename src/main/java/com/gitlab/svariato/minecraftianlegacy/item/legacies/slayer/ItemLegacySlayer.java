package com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer;

import com.gitlab.svariato.minecraftianlegacy.item.legacies.ActivatableLegacy;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.eventbus.api.Event;
import org.apache.logging.log4j.LogManager;

import java.util.UUID;

public class ItemLegacySlayer extends ActivatableLegacy
{
    public ItemLegacySlayer(Item.Properties properties)
    {
        super(properties);
    }

    @Override
    public void onActivate(PlayerEntity player, ItemStack legacyItemStack)
    {
        LogManager.getLogger().debug("Activated Legacy of the Slayer.");

        AttributeModifier mod = new AttributeModifier(UUID.fromString("e989011c-c7a8-42e0-92d1-ad8d9f88ae91"), "generic.maxHealth", (double)2.0f, AttributeModifier.Operation.ADDITION);
        player.getAttribute(SharedMonsterAttributes.MAX_HEALTH).applyModifier(mod);
    }

    @Override
    public void onDeactivate(PlayerEntity player, ItemStack legacyItemStack)
    {
        LogManager.getLogger().debug("Deactivated Legacy of the Slayer.");

        player.getAttribute(SharedMonsterAttributes.MAX_HEALTH).removeModifier(UUID.fromString("e989011c-c7a8-42e0-92d1-ad8d9f88ae91"));
    }

    @Override
    public boolean hasEffect(ItemStack stack) { return true; }
}
