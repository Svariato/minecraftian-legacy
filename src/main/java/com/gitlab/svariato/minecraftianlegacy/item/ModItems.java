package com.gitlab.svariato.minecraftianlegacy.item;

import com.gitlab.svariato.minecraftianlegacy.MinecraftianLegacy;
import com.gitlab.svariato.minecraftianlegacy.init.ModItemGroups;

import com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector.ItemInertLegacyProspector;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector.ItemLegacyProspector;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector.ItemLostLegacyProspector;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer.ItemInertLegacySlayer;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer.ItemLegacySlayer;
import com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer.ItemLostLegacySlayer;
import net.minecraft.item.Item;

import net.minecraft.item.Items;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModItems
{
    public static final DeferredRegister<Item> ITEM_REGISTER = new DeferredRegister<>(ForgeRegistries.ITEMS, MinecraftianLegacy.MODID);

    public static RegistryObject<Item> LOST_LEGACY = ITEM_REGISTER.register("lost_legacy",
            () -> new ItemLostLegacy(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));

    // Vanilla Lost Legacies
    public static RegistryObject<Item> LOSTLEGACY_COLLECTOR = ITEM_REGISTER.register("lostlegacy_collector",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.ENCHANTED_GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.GLISTERING_MELON_SLICE, Items.SADDLE, Items.NAME_TAG } ));

    public static RegistryObject<Item> LOSTLEGACY_COMBATANT = ITEM_REGISTER.register("lostlegacy_combatant",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.DIAMOND_HELMET, Items.DIAMOND_CHESTPLATE, Items.DIAMOND_LEGGINGS, Items.DIAMOND_BOOTS, Items.DIAMOND_SWORD, Items.SHIELD, Items.BOW, Items.CROSSBOW, Items.ARROW }));

    public static RegistryObject<Item> LOSTLEGACY_GATHERER = ITEM_REGISTER.register("lostlegacy_gatherer",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.WHEAT, Items.POTATO, Items.CARROT, Items.BEETROOT, Items.PUMPKIN, Items.APPLE, Items.MELON_SLICE, Items.SWEET_BERRIES, Items.COCOA_BEANS }));

    public static RegistryObject<Item> LOSTLEGACY_GOURMET = ITEM_REGISTER.register("lostlegacy_gourmet",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.MUSHROOM_STEW, Items.RABBIT_STEW, Items.BEETROOT_SOUP, Items.PUMPKIN_PIE, Items.BREAD, Items.COOKIE, Items.CAKE }));

    public static RegistryObject<Item> LOSTLEGACY_HUNTER = ITEM_REGISTER.register("lostlegacy_hunter",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.BEEF, Items.PORKCHOP, Items.CHICKEN, Items.RABBIT, Items.MUTTON, Items.LEATHER, Items.FEATHER, Items.RABBIT_HIDE, Items.RABBIT_FOOT }));

    public static RegistryObject<Item> LEGACY_PROSPECTOR = ITEM_REGISTER.register("legacy_prospector",
            () -> new ItemLegacyProspector(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));
    public static RegistryObject<Item> INERTLEGACY_PROSPECTOR = ITEM_REGISTER.register("inertlegacy_prospector",
            () -> new ItemInertLegacyProspector(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));
    public static RegistryObject<Item> LOSTLEGACY_PROSPECTOR = ITEM_REGISTER.register("lostlegacy_prospector",
            () -> new ItemLostLegacyProspector(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));

    public static RegistryObject<Item> LOSTLEGACY_SEA = ITEM_REGISTER.register("lostlegacy_sea",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.COD, Items.SALMON, Items.TROPICAL_FISH, Items.PUFFERFISH, Items.INK_SAC, Items.TURTLE_HELMET, Items.PRISMARINE_CRYSTALS, Items.TRIDENT }));

    public static RegistryObject<Item> LEGACY_SLAYER = ITEM_REGISTER.register("legacy_slayer",
            () -> new ItemLegacySlayer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));
    public static RegistryObject<Item> INERTLEGACY_SLAYER = ITEM_REGISTER.register("inertlegacy_slayer",
            () -> new ItemInertLegacySlayer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));
    public static RegistryObject<Item> LOSTLEGACY_SLAYER = ITEM_REGISTER.register("lostlegacy_slayer",
            () -> new ItemLostLegacySlayer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP)));

    public static RegistryObject<Item> LOSTLEGACY_TOOLMAKER = ITEM_REGISTER.register("lostlegacy_toolmaker",
            () -> new ItemLostLegacyWithContainer(new Item.Properties().group(ModItemGroups.MOD_ITEM_GROUP),
                    new Item[] { Items.DIAMOND_PICKAXE, Items.DIAMOND_AXE, Items.DIAMOND_SHOVEL, Items.DIAMOND_HOE }));
}
