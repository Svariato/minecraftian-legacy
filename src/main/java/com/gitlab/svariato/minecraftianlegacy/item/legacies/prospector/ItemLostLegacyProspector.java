package com.gitlab.svariato.minecraftianlegacy.item.legacies.prospector;

import com.gitlab.svariato.minecraftianlegacy.item.ItemLostLegacy;
import com.gitlab.svariato.minecraftianlegacy.item.ItemLostLegacyWithContainer;
import com.gitlab.svariato.minecraftianlegacy.item.ModItems;
import net.minecraft.item.Item;
import net.minecraft.item.Items;

public class ItemLostLegacyProspector extends ItemLostLegacyWithContainer
{
    private static final Item[] ITEMS_TO_COMPLETE = {
            Items.COAL,
            Items.IRON_INGOT,
            Items.GOLD_INGOT,
            Items.LAPIS_LAZULI,
            Items.DIAMOND,
            Items.EMERALD
    };

    public ItemLostLegacyProspector(Item.Properties properties)
    {
        super(properties, ModItems.INERTLEGACY_PROSPECTOR.get(), ITEMS_TO_COMPLETE);
    }
}
