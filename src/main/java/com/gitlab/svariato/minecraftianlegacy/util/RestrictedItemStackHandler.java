package com.gitlab.svariato.minecraftianlegacy.util;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;

/**
 * ItemStackHandler that maintains the following restrictions:
 *      - Each slot is restricted to holding one specific Item type.
 *      - Maximum stack size of one per slot.
 *      - Items cannot be damaged.
 *
 * When used with normal SlotItemHandlers, these rules will be enforced.
 */
public class RestrictedItemStackHandler extends ItemStackHandler
{
    private final Item[] restrictedItems;

    public RestrictedItemStackHandler(Item... restrictedItems)
    {
        super(restrictedItems.length);

        this.restrictedItems = restrictedItems;
    }

    public Item[] getRestrictedItems() { return restrictedItems; }

    @Override
    public int getSlotLimit(int slot) { return 1; }

    @Override
    public boolean isItemValid(int slot, @Nonnull ItemStack stack) { return !stack.isDamaged() && restrictedItems[slot] == stack.getItem(); }
}
