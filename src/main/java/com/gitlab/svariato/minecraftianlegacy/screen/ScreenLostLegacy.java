package com.gitlab.svariato.minecraftianlegacy.screen;

import com.gitlab.svariato.minecraftianlegacy.MinecraftianLegacy;
import com.gitlab.svariato.minecraftianlegacy.container.ContainerLostLegacy;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

import java.util.HashMap;
import java.util.Map;

public class ScreenLostLegacy extends ContainerScreen<ContainerLostLegacy>
{
    private static final int TARGET_COLOR = 0x4013C90A;

    private static final Map<Integer, ResourceLocation> guiTextures = new HashMap<Integer, ResourceLocation>()
    {{
        put(1, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_1.png"));
        put(2, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_2.png"));
        put(3, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_3.png"));
        put(4, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_4.png"));
        put(5, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_5.png"));
        put(6, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_6.png"));
        put(7, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_7.png"));
        put(8, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_8.png"));
        put(9, new ResourceLocation(MinecraftianLegacy.MODID, "textures/gui/lost_legacy_9.png"));
    }};

    public ScreenLostLegacy(ContainerLostLegacy container, PlayerInventory playerInventory, ITextComponent name)
    {
        super(container, playerInventory, name);
        this.xSize = 176;
        this.ySize = 133;

    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground();

        super.render(mouseX, mouseY, partialTicks);

        // Render "ghost" items.
        Item[] restrictedItems = container.getSavedInventory().getRestrictedItems();
        for (int i = 36; i < container.inventorySlots.size(); i++)
        {
            Slot slot = container.getSlot(i);

            // Get position relative to GUI.
            int x = this.guiLeft + slot.xPos;
            int y = this.guiTop + slot.yPos;

            // If the item is in the slot, highlight it green.
            if (slot.getStack() != ItemStack.EMPTY)
            {
                AbstractGui.fill(x, y, x + 16, y + 16, 805371648);
            }
            // Otherwise, highlight it red and wash it out a bit.
            else
            {
                // Fill background with red.
                AbstractGui.fill(x, y, x + 16, y + 16, 822018048);

                // Render ghost item.
                ItemStack ghostStack = restrictedItems[i - 36].getItem().getDefaultInstance();
                this.itemRenderer.renderItemAndEffectIntoGUI(getMinecraft().player, ghostStack, x, y);

                // Add a bit of washout.
                RenderSystem.depthFunc(516);
                AbstractGui.fill(x, y, x + 16, y + 16, 822083583);
                RenderSystem.depthFunc(515);
            }
        }

        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        this.font.drawString(this.container.getItemStack().getDisplayName().getFormattedText(), 8.0F, 6.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, (float)(this.ySize - 96 + 2), 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        RenderSystem.color4f(1.0f, 1.0f, 1.0f, 1.0f);
        Minecraft.getInstance().getTextureManager().bindTexture(guiTextures.get(container.getSavedInventory().getSlots()));
        this.blit(guiLeft, guiTop, 0, 0, xSize, ySize);
    }
}
