package com.gitlab.svariato.minecraftianlegacy.effect;

import com.gitlab.svariato.minecraftianlegacy.MinecraftianLegacy;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;

public class ModEffects
{
    public static final Effect PROSPECTOR_EFFECT = new EffectProspector();

    @SubscribeEvent
    public static void registerEffects(RegistryEvent.Register<Effect> event)
    {
        IForgeRegistry<Effect> registry = event.getRegistry();
        registry.register(PROSPECTOR_EFFECT.setRegistryName(new ResourceLocation(MinecraftianLegacy.MODID, "prospector_effect")));
    }
}
