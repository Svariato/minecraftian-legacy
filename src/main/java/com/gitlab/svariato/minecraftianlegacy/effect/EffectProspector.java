package com.gitlab.svariato.minecraftianlegacy.effect;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;

import java.util.UUID;

public class EffectProspector extends Effect
{
    public EffectProspector()
    {
        super(EffectType.BENEFICIAL, 14270531);
        addAttributesModifier(SharedMonsterAttributes.ATTACK_SPEED, UUID.randomUUID().toString(), (double)0.1f, AttributeModifier.Operation.MULTIPLY_TOTAL);
    }
}
