package com.gitlab.svariato.minecraftianlegacy;

import com.gitlab.svariato.minecraftianlegacy.container.ModContainers;
import com.gitlab.svariato.minecraftianlegacy.screen.ScreenLostLegacy;
import net.minecraft.client.gui.ScreenManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = MinecraftianLegacy.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientHandler
{
    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event)
    {
        ScreenManager.registerFactory(ModContainers.LOST_LEGACY, ScreenLostLegacy::new);
    }
}
