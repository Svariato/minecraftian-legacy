package com.gitlab.svariato.minecraftianlegacy.container;

import com.gitlab.svariato.minecraftianlegacy.item.ItemLostLegacyWithContainer;
import com.gitlab.svariato.minecraftianlegacy.util.RestrictedItemStackHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public class ContainerLostLegacy extends Container
{
    private static final SoundEvent COMPLETION_SOUND_EVENT = new SoundEvent(new ResourceLocation("minecraftianlegacy", "complete_legacy"));

    private ItemStack itemStack; // ItemStack this container is being opened from.
    private Hand handUsedFrom; // Hand that the ItemStack was right-clicked with.
    protected RestrictedItemStackHandler inventory; // Serializable inventory for this "handheld" container.

    public ContainerLostLegacy(int id, PlayerInventory playerInventory)
    {
        super(ModContainers.LOST_LEGACY, id);

        // Ensure we are getting the ItemStack, whether it's in the right hand or the left.
        itemStack = playerInventory.player.getHeldItemMainhand();
        handUsedFrom = Hand.MAIN_HAND;
        if (itemStack.isEmpty() || !(itemStack.getItem() instanceof ItemLostLegacyWithContainer))
        {
            // Main hand doesn't have the ItemStack, so it must be in the off hand.
            itemStack = playerInventory.player.getHeldItemOffhand();
            handUsedFrom = Hand.OFF_HAND;
        }

        // Create new ItemStackHandler to handle all the ItemStacks in this Container.
        inventory = ItemLostLegacyWithContainer.getHandlerForContainer(itemStack);

        // Set up player inventory.
        for(int i = 0; i < 3; ++i) {
            for(int j = 0; j < 9; ++j) {
                this.addSlot(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, i * 18 + 51));
            }
        }

        for(int i = 0; i < 9; ++i) {
            this.addSlot(new Slot(playerInventory, i, 8 + i * 18, 109));
        }

        // Set up item inventory based on number of slots.
        int startingPosition = 1 + ((176 - 18 * inventory.getSlots()) / 2);
        for (int i = 0; i < inventory.getSlots(); i++) addSlot(new SlotItemHandler(inventory, i, startingPosition + i * 18, 20));
    }

    public RestrictedItemStackHandler getSavedInventory() { return inventory; }
    public ItemStack getItemStack() { return itemStack; }

    @Override
    public boolean canInteractWith(PlayerEntity player)
    {
        return !itemStack.isEmpty() && player.getHeldItem(handUsedFrom).getItem() instanceof ItemLostLegacyWithContainer;
    }

    @Nonnull
    @Override
    public ItemStack transferStackInSlot(PlayerEntity player, int slotIndex)
    {
        int playerInventorySize = player.inventory.mainInventory.size();

        Slot clickedSlot = getSlot(slotIndex);

        ItemStack stack = clickedSlot.getStack();
        ItemStack newStack = stack.copy();

        if (!clickedSlot.getHasStack()) return ItemStack.EMPTY;

        // Handle shift-click on Player's inventory.
        if (slotIndex < playerInventorySize)
        {
            // Attempt to find a valid slot for this item among the Container's slots.
            for (int i = playerInventorySize; i < inventorySlots.size(); i++)
            {
                Slot possibleSlot = getSlot(i);
                if (possibleSlot.isItemValid(stack))
                {
                    // Valid slot found, attempt to fill.
                    if (!mergeItemStack(stack, i, i + 1, false)) return ItemStack.EMPTY;
                }
            }

            // No valid slots found.
            return ItemStack.EMPTY;
        }

        // Handle shift-click on Container's inventory.
        else
        {
            if (!mergeItemStack(stack, 0, playerInventorySize, true)) return ItemStack.EMPTY;
            else clickedSlot.onSlotChanged();
        }

        if (stack.isEmpty()) clickedSlot.putStack(ItemStack.EMPTY);
        else clickedSlot.onSlotChanged();

        return clickedSlot.onTake(player, newStack);
    }

    @Override
    public void onContainerClosed(PlayerEntity player)
    {
        if (isCompletedContainer())
        {
            // Replace the item in the inventory with the transformed item.
            Item transformedItem = ((ItemLostLegacyWithContainer)itemStack.getItem()).getTransformedItem();
            ItemStack stack;
            if (transformedItem == null) stack = ItemStack.EMPTY;
            else stack = new ItemStack(transformedItem);
            player.setHeldItem(handUsedFrom, stack);

            // Play completion sound effect.
            player.getEntityWorld().playSound(player, player.getPosition(), COMPLETION_SOUND_EVENT, SoundCategory.NEUTRAL, 1, 1);
        }
        else
        {
            // Serialize data and save it to this itemStack for future use.
            if (!itemStack.hasTag()) itemStack.setTag(new CompoundNBT());
            itemStack.getTag().put("lost_legacy_container_inventory", inventory.serializeNBT());
        }

        super.onContainerClosed(player);
    }

    // Helper function to determine if all slots are filled with the correct items.
    private boolean isCompletedContainer()
    {
        for (int i = 0; i < inventory.getSlots(); i++)
        {
            ItemStack stack = inventory.getStackInSlot(i);
            if (stack == ItemStack.EMPTY || !inventory.isItemValid(i, stack)) return false;
        }
        return true;
    }
}
