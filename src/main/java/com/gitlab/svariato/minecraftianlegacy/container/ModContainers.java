package com.gitlab.svariato.minecraftianlegacy.container;

import com.gitlab.svariato.minecraftianlegacy.MinecraftianLegacy;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class ModContainers
{
    public static final ContainerType<ContainerLostLegacy> LOST_LEGACY = new ContainerType<>(ContainerLostLegacy::new);

    @SubscribeEvent
    public static void onRegisterContainers(RegistryEvent.Register<ContainerType<?>> event)
    {
        IForgeRegistry<ContainerType<?>> registry = event.getRegistry();

        register(registry, "lost_legacy", LOST_LEGACY);
    }

    private static void register(IForgeRegistry<ContainerType<?>> registry, String name, IForgeRegistryEntry<ContainerType<?>> containerType)
    {
        registry.register(containerType.setRegistryName(new ResourceLocation(MinecraftianLegacy.MODID, name)));
    }
}
