package com.gitlab.svariato.minecraftianlegacy;

import com.gitlab.svariato.minecraftianlegacy.container.ModContainers;
import com.gitlab.svariato.minecraftianlegacy.effect.ModEffects;
import com.gitlab.svariato.minecraftianlegacy.item.ModItems;

import com.gitlab.svariato.minecraftianlegacy.item.legacies.slayer.ItemInertLegacySlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;

import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(MinecraftianLegacy.MODID)
public final class MinecraftianLegacy
{
    public static final String MODID = "minecraftianlegacy";

    private static final Logger LOGGER = LogManager.getLogger();

    public MinecraftianLegacy()
    {
        LOGGER.debug("Hello from Minecraftian Legacy!");

        IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();

        modBus.register(ClientHandler.class);
        modBus.register(ModItems.class);
        ModItems.ITEM_REGISTER.register(modBus);
        modBus.register(ModEffects.class);
        modBus.register(ModContainers.class);

        // Register events.
        IEventBus forgeBus = MinecraftForge.EVENT_BUS;
        forgeBus.addListener(ItemInertLegacySlayer::onLivingDeath);
    }
}
