package com.gitlab.svariato.minecraftianlegacy.init;

import com.gitlab.svariato.minecraftianlegacy.MinecraftianLegacy;
import com.gitlab.svariato.minecraftianlegacy.item.ModItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import java.util.function.Supplier;

public final class ModItemGroups
{
    public static final ItemGroup MOD_ITEM_GROUP = new ModItemGroup(MinecraftianLegacy.MODID, () -> new ItemStack(ModItems.LOST_LEGACY.get()));

    public static class ModItemGroup extends ItemGroup
    {
        private final Supplier<ItemStack> iconSupplier;

        public ModItemGroup(final String name, final Supplier<ItemStack> iconSupplier)
        {
            super(name);
            this.iconSupplier = iconSupplier;
        }

        @Override
        public ItemStack createIcon()
        {
            return iconSupplier.get();
        }
    }
}
